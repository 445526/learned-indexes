from utils import load_yaml, save_yaml, write_to_file, create_dir
from data.DataLoader import ProfisetDataLoader, CoPhIRDataLoader
import time
from indexes.Mtree import Mtree
from Experiment import Evaluator
import os

config = load_yaml('./config/config.yml')
if config['data']['dataset'] == 'PROFISET':
    loader = ProfisetDataLoader(config['data'])
elif config['data']['dataset'] == 'COPHIR':
    loader = CoPhIRDataLoader(config)
start = time.time()
df = loader.load_descriptors(normalize=False)
labels = loader.load_labels()
loading_time = time.time() - start

pivots_df = loader.load_mtree_pivots()
index = Mtree(df, labels, pivots_df, config['data']['dataset'])

MODEL = 'Mtree'

if 'PBS_JOBID' in os.environ:
    job_id = os.environ['PBS_JOBID']
else:
    job_id = None

create_dir(os.path.join(config['experiment']['output-dir'], job_id))
write_to_file(f'{job_id}/times.csv', 'loading')
write_to_file(f'{job_id}/times.csv', f'{loading_time}')

knns = loader.get_knn_ground_truth()
queries = loader.get_queries()

evaluator = Evaluator(index, knns, queries, config, job_id=job_id)
evaluator.bucket_occupancy()
evaluator.run_evaluate()
evaluator.generate_summary()

save_yaml(config, f'{job_id}/used-config.yml')
