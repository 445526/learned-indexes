from utils import write_to_file, isfile, get_current_datetime, create_dir, file_exists
import numpy as np
from indexes import BaseIndex
import pandas as pd
from typing import Dict, Tuple, List
import json
from os import path
import logging


class Evaluator:
    """ Runs an LMI experiment, evaluates it and dumps experiment info."""
    def __init__(
        self,
        index: BaseIndex,
        knns: Dict[str, Dict],
        queries: List,
        config: Dict[str, str],
        n_knns=None,
        job_id=None
    ):
        """
        Arguments:
            index (BaseIndex):
                Trained index - LMI, or instantiated - M-index or M-tree
            job_id (str):
                Identifier of the experiment run
            config (Dict[str, str]):
                Experiment's configuration
        """
        self.index = index

        assert len(set(knns.keys())) >= len(queries), \
            "`queries` file is larger than the `knns` file, hence not all queries can be evaluated."
        self.gt_knns = knns
        self.queries = queries

        self.stop_conditions_perc = config['experiment']['search-stop-conditions']
        self.stop_conditions = [
            int(index.data.X.shape[0]*cond) for cond in self.stop_conditions_perc
        ]
        if n_knns:
            self.n_knns = n_knns
        else:
            self.n_knns = len(knns[list(knns.keys())[0]])
        self.model = type(self.index).__name__
        if job_id:
            self.job_id = path.join('outputs', job_id)
        else:
            self.job_id = path.join('outputs', get_current_datetime())
        create_dir(self.job_id)

        self.LOG = logging.getLogger(__name__)
        self.LOG.setLevel(logging.INFO)

    def bucket_occupancy(self):
        bucket_info = {}
        bucket_info['n_buckets'] = len(self.index.objects_in_buckets)
        bucket_values = list(self.index.objects_in_buckets.values())
        bucket_info['mean_bucket_occupancy'] = np.array(bucket_values).mean()
        bucket_info['std_bucket_occupancy'] = np.array(bucket_values).std()
        with open(f'{self.job_id}/bucket-summary.json', 'w') as f:
            json.dump(bucket_info, f, indent=4)

    def get_buckets(self, gt_knns: Dict[str, Dict[str, float]], query: str) -> Tuple[List[int], List[int]]:
        """Gets bucket identifiers of query's 30 nearest neighbors within `self.index`.
        Assumes that all of the neighbors can be found within the structure.

        Parameters:
            gt_knns (Dict[str, Dict[str, float]]): Dictionary of queries' 30 nearest neighbors
            query (str): Current query's identifier (as a string)

        Returns:
            Tuple[List[int], List[int]]:
            List of the buckets, without the nan (placeholder) values if present
            and number of their occurencies within the 30 NNs
        """
        nan_placeholder = -1
        if all([k.isnumeric() for k in gt_knns[query].keys()]):
            knn_keys = [int(k) for k in gt_knns[query].keys()]
        else:
            knn_keys = [k for k in gt_knns[query].keys()]
        assert len(knn_keys) == self.n_knns, f'Expected 30 knns for {query}, found {len(knn_keys)}.'
        try:
            knn_buckets, counts = np.unique(
                np.nan_to_num(
                    self.index.data.y.loc[knn_keys][self.index.pred_labels].to_numpy(
                        dtype=np.float16,
                        na_value=np.nan
                    ),
                    nan=nan_placeholder
                ),
                axis=0,
                return_counts=True
            )
        except KeyError:
            print(f'Some of knn_keys: {knn_keys} are not in the dataset used by LMI.')
        return [tuple([int(b) for b in bucket if b != nan_placeholder]) for bucket in knn_buckets], counts

    def run_evaluate(self):
        search_results = []
        times = []
        results_filename = f'{self.job_id}/search.csv'
        if not file_exists(results_filename):
            write_to_file(
                results_filename,
                'query,condition,knn_score,time,visited_objects'  # noqa: E231
            )
        self.LOG.info(f'Results will be saved to {self.job_id}.')

        for query_idx, (query_name, query_row) in enumerate(self.queries.iterrows()):
            
            if query_idx != 0 and query_idx % 100 == 0:
                self.LOG.info(f'Evaluated {query_idx}/{len(self.queries)} queries.')
            if query_row.shape[0] != 0:
                query = query_row
            else:
                query = query_name
            search_results, times, visited_objects_all = self.index.search(query, self.stop_conditions)
            knn_buckets, counts = self.get_buckets(self.gt_knns, str(query_name))
            for search_result, condition, time, visited_objects in zip(
                search_results, self.stop_conditions, times, visited_objects_all
            ):
                score = 0
                for bucket, n_of_occurences in zip(knn_buckets, counts):
                    if bucket in search_result:
                        score += n_of_occurences

                write_to_file(
                    f'{self.job_id}/search.csv',f'{query_name},'  # noqa: E231
                    f'{condition},{score / self.n_knns},{time},{visited_objects}'  # noqa: E231
                )

        self.LOG.info(f"Search is finished for {len(self.queries)}, results are stored in: '{self.job_id}/search.csv'")

    def generate_summary(self):
        """Creates the `summary.json` file with aggregate information about the results.
        """
        info_dict = {}
        info_dict['model'] = self.model
        info_dict['experiment'] = self.job_id
        info_dict['stop_conditions_perc'] = self.stop_conditions_perc

        assert isfile(f'{self.job_id}/search.csv'), \
            f'`{self.job_id}/search.csv` does not exist, cannot generate summary.'
        search_df = pd.read_csv(f'{self.job_id}/search.csv')
        condition_agg = search_df.groupby('condition')
        scores = condition_agg['knn_score'].mean().values
        conditions = condition_agg['condition'].mean().values
        times = condition_agg['time'].mean().values
        visited_objects = condition_agg['visited_objects'].mean().values

        info_dict['results'] = {}
        for cond, score, time, vis_obj in zip(conditions, scores, times, visited_objects):
            info_dict['results'][str(cond)] = {}
            info_dict['results'][str(cond)]['time'] = time
            info_dict['results'][str(cond)]['score'] = score
            info_dict['results'][str(cond)]['visited_objects'] = int(vis_obj)

        with open(f'{self.job_id}/summary.json', 'w') as f:
            json.dump(info_dict, f, indent=4)
