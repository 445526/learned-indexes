# Learned-indexes
![abstr](./doc/img/graphical_abstract-1.png)

## Dataset loading times


## TBD:
Dataset directory: `/storage/brno12-cerit/home/tslaninakova/data

## Paper accompanying this code
```
@article{LMI2021,
  title={Learned Metric Index - proposition of learned indexing for unstructured data},
  author={Antol, Matej and O\v{l}ha, Jaroslav and Slanin{\'a}kov{\'a}, Ter{\'e}zia and Dohnal, Vlastislav},
  journal={Information Systems},
  volume={0},
  number={to appear},
  pages={1--13},
  year={2021},
  publisher={Elsevier}
}
```
Link to the Elsevier's Information Systems issue will be provided once the journal is published.

## Intro
This repository contains the code necessary to build with an example CoPhIR dataset of 100k objects included.
Currently supported ML models:
- Fully-connected NNs
- Logistic regression
- Fully-connected NNs trained in a Multi-label fashion
- Random Forests

## How to run
- Download the repo and run the code locally
    - python 3.x required
    - run `pip install -r requirements.txt` to install needed dependencies

## Introductory notebooks:
- [Dataset exploration + GMM](https://gitlab.ics.muni.cz/445526/learned-indexes/-/blob/master/Dataset%20exploration%20+%20GMM.ipynb)
- [LMI Playground](https://gitlab.ics.muni.cz/445526/learned-indexes/-/blob/master/LMI%20Playground.ipynb)