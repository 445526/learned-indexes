import pytest
from indexes.LearnedMetricIndex import LMI
import numpy as np
import pandas as pd
from utils import load_model_config, isfile
from Experiment import Evaluator
from visualization import Plot
import os


def get_synthetic_object(dim=284):
    return np.random.randn(dim, )


def get_synthetic_descriptors(n_data_points, dim=284):
    return pd.DataFrame(np.random.randn(dim, n_data_points).T)


@pytest.fixture
def synthetic_query():
    return pd.DataFrame(index=[0])


@pytest.fixture
def synthetic_query_descriptor(dim=284):
    query_descriptor = np.random.randn(dim, 1).T
    return pd.DataFrame(query_descriptor)


@pytest.fixture
def synthetic_knn_gt():
    return {'0': {'1': 0, '2': 0, '3': 0}}


@pytest.fixture
def synthetic_config():
    return {'experiment': {'search-stop-conditions': [0.5]}}


@pytest.fixture
def get_model_config_gmm():
    return load_model_config('config/model-gmm.yml', 3)


@pytest.fixture
def synthetic_data(n_datapoints=100, n_levels=3):
    descriptors = get_synthetic_descriptors(n_datapoints)
    return descriptors, n_levels


def test_experiment_is_evaluated(
    synthetic_data,
    synthetic_query,
    synthetic_query_descriptor,
    synthetic_knn_gt,
    synthetic_config,
    get_model_config_gmm
):
    n_levels = synthetic_data[1]
    lmi_config = {'n_levels': n_levels, 'training-dataset-percentage': 1}
    lmi = LMI(lmi_config, synthetic_data[0])
    lmi.train(get_model_config_gmm)
    query = synthetic_query
    query_descriptor = synthetic_query_descriptor
    knn_gt = synthetic_knn_gt

    for q in [query, query_descriptor]:
        e = Evaluator(lmi, knn_gt, q, synthetic_config)
        e.run_evaluate()
        e.generate_summary()

        assert isfile(os.path.join(e.job_id, 'summary.json'))
        assert isfile(os.path.join(e.job_id, 'search.csv'))

        plot = Plot([e.job_id])
        plot.get_experiment_infos()
        plot.plot_experiments(save=True)

        assert isfile(os.path.join(e.job_id, 'stop-cond-recall.png'))
        assert isfile(os.path.join(e.job_id, 'time-recall.png'))
