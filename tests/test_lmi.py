import pytest
from indexes.LearnedMetricIndex import LMI
import numpy as np
import pandas as pd
from utils import load_model_config


def get_synthetic_object(dim=284):
    return np.random.randn(dim, )


def get_synthetic_descriptors(n_data_points, dim=284):
    return pd.DataFrame(np.random.randn(dim, n_data_points).T)


def get_synthetic_labels(n_data_points, n_levels):
    labels = ['L1', 'L2', 'L3', 'L4', 'L5', 'L6', 'L7', 'L8'][:n_levels]
    return pd.DataFrame(
        np.array(
            [np.random.randint(0, n_data_points % 120, n_data_points) for _ in range(n_levels)]
        ).T, columns=labels[:n_levels])


def get_synthetic_multilabels(n_data_points, n_levels, n_knns=30):
    labels = ['L1', 'L2', 'L3', 'L4', 'L5', 'L6', 'L7', 'L8']
    target_labels = [
        [list(np.random.randint(0, n_data_points % 120, n_knns)) for _ in range(n_data_points)]
        for _ in range(n_levels)
    ]
    data = {}
    for i, label in enumerate(labels[:n_levels]):
        data[label] = target_labels[i]
    return pd.DataFrame(data, columns=labels[:n_levels])


def get_mindex_synthetic_labels(n_data_points, n_levels):
    """ Simulates synthetic M-index labels (with missing values)
    Produces following type of arrays:
    array([[ 2.,  1.,  2., nan],
           [ 2.,  3.,  1.,  4.],
           [ 3.,  0.,  1., nan],
           [ 1.,  0.,  4.,  4.],
           [ 2., nan, nan, nan]])

    Where np.nan occurs with similar frequency than in real M-index labels (given by `prob_nan_on_levels`)
    and if a np.nan is present in datapoint in one level, it will be present in the next one as well (`prev_nan`).
    """
    labels_cols = ['L1', 'L2', 'L3', 'L4', 'L5', 'L6', 'L7', 'L8']

    labels = np.empty((n_data_points, n_levels))
    labels[:] = np.NaN
    # no np.nan on the first level
    labels[:, 0] = np.random.randint(0, n_data_points % 120, n_data_points)

    n_cats = n_data_points % 120 + 1
    prob_nan_on_levels = [0.15, 0.35 - 0.15,
                          0.7 - (0.35 - 0.15),
                          0.8 - (0.7 - (0.35 - 0.15)),
                          0.9 - (0.8 - (0.7 - (0.35 - 0.15)))]
    nan_int_label = n_cats-1
    prev_nan = np.array([])
    for level, prob_na in zip(range(1, n_levels), prob_nan_on_levels):
        uniform_prob = (1-prob_na) / (n_cats-1)
        # generate the int labels with uniform probability except for the last one (representing np.nan)
        col1 = np.random.choice(
            n_cats,
            n_data_points,
            p=[uniform_prob if i != n_cats-1 else prob_na for i, cat in enumerate(range(n_cats))]
        )
        # assign np.nan in lieu of `nan_int_label`
        labels[np.where(col1 != nan_int_label)[0], level] = col1[np.where(col1 != nan_int_label)[0]]
        if prev_nan.size != 0:
            labels[prev_nan, level] = np.nan
        # remember where we put np.nan in this iteration
        prev_nan = np.where(np.isnan(labels[:, level]))[0]
    df_raw = pd.DataFrame(labels, columns=labels_cols[:n_levels])

    for col, dtype in df_raw.dtypes.items():
        if col == 'L1':
            df_raw[col] = df_raw[col].astype(int)
        elif dtype != np.uint32:
            df_raw[col] = df_raw[col].astype(pd.UInt16Dtype())
    return df_raw


@pytest.fixture
def synthetic_query():
    return [0]


@pytest.fixture
def get_model_config_logreg():
    return load_model_config('config/model-logreg.yml', 8)


@pytest.fixture
def get_model_config_rf():
    return load_model_config('config/model-rf.yml', 8)


@pytest.fixture
def get_model_config_nn():
    return load_model_config('config/model-nn.yml', 8)


@pytest.fixture
def get_model_config_multilabel_nn():
    return load_model_config('config/model-multilabel-nn.yml', 8)


@pytest.fixture
def get_model_config_gmm():
    return load_model_config('config/model-gmm.yml', 8)


@pytest.fixture
def get_model_config_bayesian_gmm():
    return load_model_config('config/model-bayesian-gmm.yml', 8)


@pytest.fixture
def get_model_config_kmeans():
    return load_model_config('config/model-kmeans.yml', 8)


@pytest.fixture
def get_model_config_logreg_kmeans():
    return load_model_config('config/model-logreg-kmeans.yml', 8)


@pytest.fixture
def synthetic_data(n_datapoints=100, n_levels=7):
    descriptors = get_synthetic_descriptors(n_datapoints)
    labels = get_synthetic_labels(n_datapoints, n_levels)
    return descriptors, labels, n_levels


@pytest.fixture
def synthetic_multialbels(n_datapoints=100, n_levels=7):
    labels = get_synthetic_multilabels(n_datapoints, n_levels)
    return labels


@pytest.fixture
def synthetic_data_mindex(n_datapoints=50, n_levels=7):
    descriptors = get_synthetic_descriptors(n_datapoints)
    labels = get_mindex_synthetic_labels(n_datapoints, n_levels)
    return descriptors, labels, n_levels


def test_supervised_lmi_mindex_is_built_can_search(
    synthetic_data_mindex,
    get_model_config_logreg,
    synthetic_query
):
    query = synthetic_query
    n_datapoints = synthetic_data_mindex[0].shape[0]
    n_levels = synthetic_data_mindex[2]
    lmi_config = {'n_levels': n_levels, 'training-dataset-percentage': 0.5}
    lmi = LMI(lmi_config, synthetic_data_mindex[0], synthetic_data_mindex[1])

    lmi.train(get_model_config_logreg)
    assert sum(lmi.objects_in_buckets.values()) == n_datapoints
    assert len(lmi.model_stack) == n_levels
    assert len(lmi.model_mapping) == n_levels-1

    assert lmi.n_levels == n_levels

    lmi.search(query, [n_datapoints // 2])


def test_supervised_lmi_is_built_can_search(
    synthetic_data,
    synthetic_multialbels,
    synthetic_query,
    get_model_config_logreg,
    get_model_config_rf,
    get_model_config_nn,
    get_model_config_multilabel_nn
):
    query = synthetic_query
    n_datapoints = synthetic_data[0].shape[0]
    n_levels = synthetic_data[2]
    training_dataset_percentages = [0.5, 1]
    for training_dataset_percentage in training_dataset_percentages:
        lmi_config = {
            'n_levels': n_levels,
            'training-dataset-percentage': training_dataset_percentage
        }
        for model_config in [
            get_model_config_logreg,
            get_model_config_rf,
            get_model_config_nn,
            get_model_config_multilabel_nn
        ]:
            if model_config['level-0']['model'] == 'MultilabelNN':
                lmi = LMI(lmi_config, synthetic_data[0], synthetic_multialbels.copy())
            else:
                lmi = LMI(lmi_config, synthetic_data[0], synthetic_data[1].copy())

            lmi.train(model_config)
            assert lmi.data.X.shape[0] == n_datapoints
            assert lmi.train_data.X.shape[0] == n_datapoints * lmi_config['training-dataset-percentage']
            assert sum(lmi.objects_in_buckets.values()) == n_datapoints
            assert len(lmi.model_stack) == n_levels
            assert len(lmi.model_mapping) == n_levels-1

            assert lmi.n_levels == n_levels

            lmi.search(query, [n_datapoints // 2])


def test_unsupervised_lmi_is_built_can_search(
    synthetic_data,
    synthetic_query,
    get_model_config_gmm,
    get_model_config_bayesian_gmm,
    get_model_config_kmeans,
    get_model_config_logreg_kmeans
):
    query = synthetic_query
    n_datapoints = synthetic_data[0].shape[0]
    n_levels = 8
    training_dataset_percentages = [0.5, 1]
    for training_dataset_percentage in training_dataset_percentages:
        lmi_config = {
            'n_levels': n_levels,
            'training-dataset-percentage': training_dataset_percentage
        }
        for model_config in [
            get_model_config_gmm,
            get_model_config_bayesian_gmm,
            get_model_config_kmeans,
            get_model_config_logreg_kmeans
        ]:
            lmi = LMI(lmi_config, synthetic_data[0])
            lmi.train(model_config)

            assert sum(lmi.objects_in_buckets.values()) == n_datapoints
            assert len(lmi.model_stack) == n_levels
            assert len(lmi.model_mapping) == n_levels-1

            assert lmi.n_levels == n_levels

            lmi.search(query, [n_datapoints // 2])
