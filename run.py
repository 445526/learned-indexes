from utils import load_yaml, save_yaml, load_model_config, write_to_file, create_dir
from data.DataLoader import ProfisetDataLoader, CoPhIRDataLoader
import time
from indexes.LearnedMetricIndex import LMI
from Experiment import Evaluator
import os
from visualization import Plot

config = load_yaml('./config/config.yml')
if 'PBS_JOBID' in os.environ:
    job_id = os.environ['PBS_JOBID']
else:
    job_id = None

if config['data']['dataset'] == 'PROFISET':
    loader = ProfisetDataLoader(config)
elif config['data']['dataset'] == 'COPHIR':
    loader = CoPhIRDataLoader(config)

start = time.time()
df = loader.load_descriptors()
labels = loader.load_labels()
loading_time = time.time() - start

lmi = LMI(config['LMI'], df, labels)

model_config = load_model_config(config['LMI']['model-config'], lmi.n_levels)
create_dir(f'outputs/{job_id}')
save_yaml(config, f'outputs/{job_id}/used-config.yml')
save_yaml(model_config, f'outputs/{job_id}/used-model-config.yml')

start = time.time()
lmi.train(model_config)
training_time = time.time() - start
lmi.LOG.info(f'n. objects: {len(lmi.objects_in_buckets)}')

write_to_file(f'outputs/{job_id}/times.csv', 'loading, training')
write_to_file(f'outputs/{job_id}/times.csv', f'{loading_time}, {training_time}')


knns = loader.get_knn_ground_truth()
queries = loader.get_queries()
evaluator = Evaluator(lmi, knns, queries, config, job_id=job_id)
evaluator.bucket_occupancy()
evaluator.run_evaluate()
evaluator.generate_summary()

plot = Plot([evaluator.job_id])
plot.get_experiment_infos()
plot.plot_experiments(save=True)
