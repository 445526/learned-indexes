#!/bin/bash
# 
# Transforms .data CoPhIR dataset into a csv format of 284 columns.
# Creates a separate file for the object labels.
#
# ===== EXAMPLE USAGE ======
# ./cophir-to-csv.sh /path/to/cophir-1M.data
# 
# extracts the descriptors - outputs any line (with line numbers) that begins with '-' or any digit
# extracts object ids into a separate file
if [ $# -eq 0 ]
  then
    echo "No arguments supplied, exiting."
    exit
fi

base_filename="$(echo $1 | cut -d'.' -f1)"
grep -oP '^[\-|0-9].*' $1 --line-number > $base_filename-descriptors-temp.txt
grep -oP '#objectKey messif.objects.keys.(BucketId|Abstract)ObjectKey\s\K\S+' $1 > $base_filename-objects.txt
python cophir-descriptors-to-csv.py $base_filename-descriptors-temp.txt > $base_filename-descriptors-temp.csv
sed "s/;/,/g" $base_filename-descriptors-temp.csv > $base_filename-descriptors.csv
rm $base_filename-descriptors-temp.*

echo "Created the following files: $base_filename-objects.txt , $base_filename-descriptors.csv"