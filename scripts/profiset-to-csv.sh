#!/bin/bash
# 
# Transforms .data Profiset dataset into a csv format of 4096 columns,
# creates a separate file for the object labels.
#
# ===== EXAMPLE USAGE ======
# ./profiset-to-csv.sh /path/to/profiset-1M.data
# 
# extracts the descriptors - outputs any line (with line numbers) that begins with '-' or any digit
# extracts object ids into a separate file
if [ $# -eq 0 ]
  then
    echo "No arguments supplied, exiting."
    exit
fi

base_filename="$(echo $1 | cut -d'.' -f1)"
grep -oP '#objectKey messif.objects.keys.(BucketId|Abstract)ObjectKey\s\K\S+' $1 > $base_filename-objects.txt
awk 'NR%2==0' $1 > $base_filename-descriptors.csv

echo "Created the following files: $base_filename-objects.txt , $base_filename-descriptors.csv"