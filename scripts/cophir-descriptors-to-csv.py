#!/usr/bin/env python
"""
A script for getting the raw extracted descriptors into a csv form.

=========== EXAMPLE USAGE ===========
python descriptors-to-csv.py descriptors-raw.txt > descriptors.csv

=========== DESCRIPTION =============
The raw descriptor file looks like this, and is created by grepping integers in the
`cophir-to-csv.sh` script:
```
3:21, 18, 25, 18, 9, 15; 32, 16, 16; 32, 16, 16
4:0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, ...
5:6,
```
This script takes this file, joins the lines that are suppoed to represent one object
(consecutive lines based on line number: "3:, 4:,", etc.) fills in empty cells where
the 'Location' descriptor is missing and outputs (by printing) the processed .csv file.
"""
import sys
import re


def output_csv_descriptors(input_file):  # noqa: C901
    """ Collects the descriptors from multiple lines, joins them if they represent one object and prints them out.
    Handles the missing 'Location' descriptor.

    Parameters
    -------
    input_file: string
        Filename of the raw descriptors.

    """
    LOCATION_DESCRIPTOR_POSITION = 5
    GPS_DESCRIPTOR_POSITION = 6
    LOCATION_DESCRIPTOR_LENGTH = 2
    N_COLUMNS = 284

    def is_line_number_consecutive(prev_line_number, line_number):
        if prev_line_number is None:
            return True
        elif prev_line_number is not None and int(prev_line_number) + 1 == int(line_number):
            return True
        else:
            return False

    def read_next_line(file):
        return file.readline().rstrip('\n')

    def get_plain_descriptor(line):
        return line[line.index(":")+1:]

    def get_line_number(line):
        return line[:line.index(":")]

    def format_final_row(current_line, gps_coords):
        current_descrs_str = ",".join(current_line)
        if gps_coords:
            current_descrs_str = re.sub(r",,,", r"," + gps_coords + ",", current_descrs_str)
        try:
            assert len(re.findall(r"[,;]", current_descrs_str)) == N_COLUMNS-1
        except AssertionError:
            sys.stderr.write(
                str(len(re.findall(r"[,;]", current_descrs_str))) +
                " columns in line " + str(counter) + ":" + current_descrs_str
            )
            exit(1)
        return current_descrs_str

    with open(input_file, 'r') as file:
        line = read_next_line(file)
        prev_line_number = None
        descr_counter = 1
        current_line = []
        counter = 0
        gps_coords = None
        while line:
            line_descr = get_plain_descriptor(line)
            if is_line_number_consecutive(prev_line_number, get_line_number(line)):
                if descr_counter == LOCATION_DESCRIPTOR_POSITION and \
                   len(line_descr.split(",")) != LOCATION_DESCRIPTOR_LENGTH:
                    current_line.append(",")
                    current_line.append(line_descr)
                elif descr_counter == GPS_DESCRIPTOR_POSITION and \
                        len(line_descr.split(",")) == LOCATION_DESCRIPTOR_LENGTH:
                    gps_coords = line_descr
                else:
                    current_line.append(line_descr)
                prev_line_number = get_line_number(line)
            else:
                current_descrs_str = format_final_row(current_line, gps_coords)
                print(current_descrs_str)
                current_line = [line_descr]
                prev_line_number = None
                gps_coords = None
                descr_counter = 1
            line = read_next_line(file)
            descr_counter += 1
            counter += 1

    current_descrs_str = format_final_row(current_line, gps_coords)
    print(current_descrs_str)


if __name__ == "__main__":
    if len(sys.argv) > 1:
        output_csv_descriptors(sys.argv[1])
    else:
        print("No argument were provided, exiting.")
        exit(1)
