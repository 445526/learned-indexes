#!/bin/bash
#PBS -l select=1:ncpus=1:mem=10gb:scratch_local=5gb
#PBS -l walltime=10:00:00
#PBS -m ae
#PBS -N lmi_training

# run_on_metacentrum_cpu.sh:
# -------------------------
# Running script for lmi training on metacentrum
#
# Parameters:
# -----------
# CODEROOT - path to the root of the code repository for LMI
#
# USAGE:
# ------
# qsub -v CODEROOT=/path/to/repo/root scripts/run_on_metacentrum_cpu.sh

cd $SCRATCHDIR

module add tensorflow-2.0.0-gpu-python3
python3 -m venv lmienv
source lmienv/bin/activate
python3 -m pip install --upgrade pip

cd $CODEROOT
export PYTHONPATH=$CODEROOT/../:$PYTHONPATH

python3 -m pip install -r requirements.txt
export TF_CPP_MIN_LOG_LEVEL=2
python3 run.py

deactivate
rm -rf $SCRATCHDIR/lmienv
