import numpy as np

PROFISET_DIMENSIONALITY = 4096


def get_euclidean_distance(object_1, object_2):
    assert object_1.shape == object_1.shape and object_1.shape[1] == PROFISET_DIMENSIONALITY
    return np.linalg.norm(object_1-object_2)
