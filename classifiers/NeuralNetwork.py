import numpy as np
from tensorflow.keras import layers, models, optimizers
from sklearn import preprocessing
from sklearn.utils.class_weight import compute_class_weight
import logging
from utils import get_logger_config, one_hot_frequency_encode
import os
import tensorflow as tf
from typing import Dict


def limit_tf_cpu_usage():
    """ Limits CPU usage to permitted value set by
        an environment variable `NCPUS`.

        Returns
        ----------
        cpu_count : str
            Number of CPUs to be used.
    """
    cpu_count = os.environ['NCPUS']
    tf.config.threading.set_inter_op_parallelism_threads(int(cpu_count))
    tf.config.threading.set_intra_op_parallelism_threads(int(cpu_count))

    os.environ['OMP_NUM_THREADS'] = cpu_count
    os.environ['TF_NUM_INTRAOP_THREADS'] = cpu_count
    os.environ['TF_NUM_INTEROP_THREADS'] = cpu_count
    return cpu_count


class LMINeuralNetwork:

    def __init__(
        self,
        node_id,
        **kwargs
    ):
        logging.basicConfig(level=logging.INFO, format=get_logger_config())
        self.LOG = logging.getLogger(__name__)
        if 'NCPUS' in os.environ:
            cpu_count = limit_tf_cpu_usage()
            self.LOG.warning(f'Set number of CPUs to be used by TensorFlow to {cpu_count}')
        self.node_id = node_id
        self.model = models.Sequential()
        self.encoder = preprocessing.LabelEncoder()
        for dense_info in kwargs['hidden_layers']['dense']:
            self.add_dense_layer(
                units=dense_info['units'],
                activation=dense_info['activation'],
                dropout=dense_info['dropout']
            )

    def add_dense_layer(self, units: int, activation='relu', dropout=None):
        """
        Adds a Dense layer to the model, potentially followed by a Dropout
        layer.

        Parameters
        ----------
        units : int
            Same as Dense's `units`.
        activation : str , optional
            Same as Dense's `activation`. Default 'relu'.
        dropout : float, optional
            Same as Dropout's `rate`, or None if no dropout is to be used.
            Default None.
        """
        self.model.add(layers.Dense(units, activation=activation))
        if dropout is not None:
            self.model.add(layers.Dropout(dropout))

    def add_output_layer(self, y: np.ndarray, activation='softmax'):
        """
        Adds the last layer.

        Parameters
        ----------
        y : np.ndarray
            Labels
        activation : str , optional
            Same as Dense's `activation`. Default 'relu'.
        """
        self.encoder.fit(y)
        self.add_dense_layer(np.unique(y).shape[0], activation)

    def compile(self, config: Dict):
        if config['optimizer'] == 'adam':
            opt = optimizers.Adam(learning_rate=config['learning_rate'])
        else:
            self.LOG.warn(f"Unknown optimizer: {config['optimizer']}")

        self.model.compile(loss=config['loss'], metrics=['accuracy'], optimizer=opt)

    def encode_input_labels(self, y: np.ndarray) -> np.ndarray:
        """
        Encodes the labels.

        Parameters
        ----------
        y : np.ndarray
            Labels

        Returns
        ----------
        np.ndarray
            Encoded labels
        """
        return self.encoder.transform(y)

    def train(
        self,
        X: np.array,
        y: np.array,
        epochs: int,
        use_class_weights=True
    ):
        """ Trains a Neural Network. Expects model_dict to contain
        hyperparameters *opt* and *ep* (depth and number of estimators)

        Parameters
        -------
        rf_model: Dict
            Dictionary of model specification
        X: Numpy array
            Training values
        y: Numpy array
            Training labels

        Returns
        -------
        predictions: Numpy array
            Array of model predictions

        encoder: LabelEncoder
            Mapping of actual labels to labels used in training
        """
        y = self.encode_input_labels(y)
        self.node_classes_ = [self.node_id + (node, ) for node in self.encoder.classes_]

        if use_class_weights:
            class_weight = compute_class_weight(class_weight='balanced', classes=y, y=np.unique(y))
        else:
            class_weight = None
        with tf.device('/CPU:0'):
            self.model.fit(X, y, epochs=epochs, class_weight=class_weight, verbose=0)
        return self.predict_classes(X)

    def predict_query(self, query: np.ndarray) -> np.ndarray:
        """ Collects predictions for a query (= one object/data point).

        Parameters
        -------
        query: np.ndarray
            Query - expected shape: (1, n_descriptors)

        Returns
        -------
        np.array[classes, probabilities]
            2D Array of classes and their assigned proabilities.
        """
        assert query.shape[0] == 1
        prob_distr = self.predict(query)[0]
        assert len(self.node_classes_) == prob_distr.shape[0]
        return np.array((self.node_classes_, prob_distr), dtype=object).T

    def predict(self, data):
        return self.model.predict(data)

    def predict_classes(self, data: np.ndarray):
        """ Given a trained classifier, predict's data classes.

        Parameters
        -------
        data : np.ndarray
            Data to be predicted
        """
        if data.shape[0] > 0:
            predictions = self.model.predict_classes(data)
            return self.encoder.inverse_transform(predictions)
        else:
            return None


class LMIMultilabelNeuralNetwork(LMINeuralNetwork):

    def __init__(
        self,
        node_id,
        **kwargs
    ):
        super().__init__(node_id, **kwargs)

    def add_output_layer(self, y: np.ndarray, activation='sigmoid'):
        """
        Adds the last layer. For multilabel training we're
        using sigmoid instead of softmax with one-hot encoded labels.

        Parameters
        ----------
        y : np.ndarray
            Labels
        activation : str , optional
            Same as Dense's `activation`. Default 'relu'.
        """
        self.labels_1d = np.concatenate(np.array([np.unique(v) for v in y], dtype=object), axis=0).astype(np.int64)
        self.y_classes = np.unique(self.labels_1d)
        self.encoder.fit(self.labels_1d)
        self.add_dense_layer(len(self.y_classes), activation)

    def encode_input_labels(self, y: np.ndarray) -> np.ndarray:
        """
        Encodes the input labels with one-hot frequency encoding.

        Parameters
        ----------
        y : np.ndarray
            Labels

        Returns
        -------
        np.ndarray
            One-hot frequency encoded labels
        """
        mapper_dict = dict(zip(self.labels_1d, self.encoder.transform(self.labels_1d)))

        def mp(entry):
            return mapper_dict[entry] if entry in mapper_dict else entry

        mp = np.vectorize(mp)
        encoded = [mp(v) for v in y]
        return one_hot_frequency_encode(encoded, len(self.encoder.classes_))
