"""
Useful enum definitions realted to classifiers.
"""
from enum import Enum
from classifiers.RandomForest import LMIRandomForest
from classifiers.LogisticRegression import LMILogisticRegression
from classifiers.NeuralNetwork import LMINeuralNetwork, LMIMultilabelNeuralNetwork
from classifiers.GMM import LMIGaussianMixtureModel, LMIBayesianGaussianMixtureModel
from classifiers.KMeans import LMIKMeans, LMIFaissKMeans
from classifiers.DummyClassifier import LMIDummyClassifier


class Classifiers(Enum):
    RF = LMIRandomForest
    LogReg = LMILogisticRegression
    NN = LMINeuralNetwork
    MultilabelNN = LMIMultilabelNeuralNetwork
    GMM = LMIGaussianMixtureModel
    BayesianGMM = LMIBayesianGaussianMixtureModel
    KMeans = LMIKMeans
    FaissKMeans = LMIFaissKMeans
    Dummy = LMIDummyClassifier
