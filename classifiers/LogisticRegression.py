import numpy as np
from sklearn.linear_model import LogisticRegression
from warnings import simplefilter
from typing import List
from sklearn.exceptions import ConvergenceWarning
simplefilter("ignore", category=ConvergenceWarning)
# Filter out convergence warning:
#   Since running LogReg on lots of data and lot of iterations
#   may be expensive, we allow the failure of convergence
#   and needn't notify about it in the logs.


class LMILogisticRegression(LogisticRegression):

    def __init__(
        self,
        node_id,
        max_iter,
        class_weight='balanced',
        solver='lbfgs',
        multi_class='ovr',
        **kwargs
    ):
        super().__init__(
            max_iter=max_iter,
            class_weight=class_weight,
            solver=solver,
            multi_class=multi_class,
            **kwargs
        )
        self.node_id = node_id

    def fit(
        self,
        X: np.array,
        y: np.array
    ) -> List[int]:
        """ Trains a Logistic regression model.
        Expects model_dict to contain hyperparameter *ep* (number of epochs)

        Parameters
        -------
        X: Numpy array
            Training values
        y: Numpy array
            Training labels

        Returns
        -------
        predictions: Numpy array
            Array of model predictions
        """
        super().fit(X, y)
        self.node_classes_ = [self.node_id + (node, ) for node in self.classes_]
        return self.predict_classes(X)

    def predict_query(self, query: np.ndarray) -> np.ndarray:
        """ Collects predictions for a query (= one object/data point).

        Parameters
        -------
        query: np.ndarray
            Query - expected shape: (1, n_descriptors)

        Returns
        -------
        np.ndarray[classes, probabilities]
            2D Array of classes and their assigned proabilities.
        """
        assert query.shape[0] == 1
        predictions = self.predict_proba(query)[0]
        assert len(self.node_classes_) == predictions.shape[0]
        return np.array((self.node_classes_, predictions), dtype=object).T

    def predict_classes(self, data: np.ndarray):
        """ Given a trained classifier, predict's data classes.

        Parameters
        -------
        data : np.ndarray
            Data to be predicted
        """
        if data.shape[0] > 0:
            return super().predict(data)
        else:
            return None
